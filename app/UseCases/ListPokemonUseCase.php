<?php

namespace App\UseCases;

use Illuminate\Support\Facades\Storage;

class ListPokemonUseCase
{
    const PAGE_SIZE = 200;

    public function handle($pageIndex)
    {
        //List all pokemons from the CSV.
        $fileContent = Storage::get('pokemon.csv');
        $arrayRows = explode("\n", $fileContent);

        $collection = collect();
        array_shift($arrayRows);
        foreach($arrayRows as $row) {
            $data = str_getcsv($row);
            if ($data[0]) {
                $collection->add($data);
            }
        }

        $chunks = $collection->chunk(self::PAGE_SIZE);
        if($pageIndex < 1) {
            $pageIndex = 1;
        } elseif($pageIndex > $chunks->count()) {
            $pageIndex = $chunks->count();
        }
        $collection = $chunks->get($pageIndex-1);

        return view('pokemons.index', ['collection' => $collection,
                                            'pageIndex' => $pageIndex,
                                            'pageCount' => $chunks->count()]);
    }
}