<?php

namespace App\UseCases;

use App\Helpers\AnonymousFunctions;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class StorePokemonUseCase
{
    public function handle(array $data)
    {
        //Add the new pokemon into the CSV at the end of the file.
        if(Arr::get($data, '_token')) {
            unset($data['_token']);
        }
        $fileContent = Storage::get('pokemon.csv');
        $arrayRows = explode("\n", $fileContent);

        array_unshift($data, end($arrayRows)[0]+1);
        Storage::put('pokemon.csv', $fileContent."\n".AnonymousFunctions::str_putcsv($data, ','));
    }
}