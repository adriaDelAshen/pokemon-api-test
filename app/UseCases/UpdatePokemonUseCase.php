<?php

namespace App\UseCases;

use App\Helpers\AnonymousFunctions;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class UpdatePokemonUseCase
{
    public function handle(array $data)
    {
        //Update the pokemon data.
        if(Arr::get($data, '_token')) {
            unset($data['_token']);
        }

        if(Arr::get($data, '_method')) {
            unset($data['_method']);
        }

        $fileContent = Storage::get('pokemon.csv');
        $arrayRows = explode("\n", $fileContent);

        foreach ($arrayRows as $index => $row) {
            if($data['id'] == str_getcsv($row)[0]) {
                $arrayRows[$index] = AnonymousFunctions::str_putcsv($data);
                break;
            }
        }

        Storage::put('pokemon.csv', implode("\n", $arrayRows));
    }
}