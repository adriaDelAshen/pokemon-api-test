<?php

namespace App\UseCases;

use Illuminate\Support\Facades\Storage;

class DestroyPokemonUseCase
{
    public function handle(array $data)
    {
        //Remove the pokemon from the CSV.
        $fileContent = Storage::get('pokemon.csv');
        $arrayRows = explode("\n", $fileContent);

        foreach ($arrayRows as $index => $row) {
            if($data['id'] == str_getcsv($row)[0]) {
                unset($arrayRows[$index]);
            }
        }

        Storage::put('pokemon.csv', implode("\n", $arrayRows));
    }
}