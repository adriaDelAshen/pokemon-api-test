<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePokemonRequest;
use App\Http\Requests\UpdatePokemonRequest;
use App\UseCases\DestroyPokemonUseCase;
use App\UseCases\ListPokemonUseCase;
use App\UseCases\StorePokemonUseCase;
use App\UseCases\UpdatePokemonUseCase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PokemonController extends Controller
{
    public function index(Request $request, ListPokemonUseCase $useCase) {
        return $useCase->handle($request->get('page', 1));
    }

    public function create() {
        return view('pokemons.create');
    }

    public function store(StorePokemonRequest $request, StorePokemonUseCase $useCase) {
        $useCase->handle($request->all());
        return redirect(route('pokemons.index'));
    }

    public function edit(Request $request) {
        $fileContent = Storage::get('pokemon.csv');
        $arrayRows = explode("\n", $fileContent);

        $collection = collect();
        foreach ($arrayRows as $index => $row) {
            $arrayCells = str_getcsv($row);
            if($request->pokemon == $arrayCells[0]) {
                $collection->add($arrayCells);
                break;
            }
        }

        return view('pokemons.edit', ['pokemon' => $collection->first()]);
    }

    public function update(UpdatePokemonRequest $request, UpdatePokemonUseCase $useCase) {
        $data = array_merge(['id' => $request->pokemon], $request->all());
        $useCase->handle($data);
        return redirect(route('pokemons.index'));
    }

    public function destroy(Request $request, DestroyPokemonUseCase $useCase) {
        $useCase->handle(['id' => $request->pokemon]);//Id is pass through the Resource.

        return redirect()->back();
    }
}
