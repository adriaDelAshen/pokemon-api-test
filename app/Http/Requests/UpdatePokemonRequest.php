<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Storage;

class UpdatePokemonRequest extends StorePokemonRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no' => 'required|numeric',
            'name' => 'required|string',
            'type_1' => 'required|string',
            'type_2' => 'present',
            'total' => 'required|numeric',
            'hp' => 'required|numeric',
            'attack' => 'required|numeric',
            'defense'=> 'required|numeric',
            'sp_atk'=> 'required|numeric',
            'sp_def'=> 'required|numeric',
            'speed'=> 'required|numeric',
            'generation'=> 'required|numeric',
            'legendary'=> 'required|boolean',
        ];
    }

    public function withValidator(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $validator->after(function ($validator) {
            //If id does not exist in CSV, we throw an exception.
            if (!$this->idExists($this->pokemon)) {
                $validator->errors()->add('id', 'Id does not exist.');
            }
        });
    }

    protected function idExists($id) {
        $fileContent = Storage::get('pokemon.csv');
        $arrayRows = explode("\n", $fileContent);

        array_shift($arrayRows);//Removing headers since we just want the ids
        foreach ($arrayRows as $row) {
            if($id == str_getcsv($row)[0]) {
                return true;
            }
        }

        return false;
    }
}
