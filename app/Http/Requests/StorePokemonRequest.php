<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePokemonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no' => 'required|numeric',
            'name' => 'required|string',
            'type_1' => 'required|string',
            'type_2' => 'present',
            'total' => 'required|numeric',
            'hp' => 'required|numeric',
            'attack' => 'required|numeric',
            'defense'=> 'required|numeric',
            'sp_atk'=> 'required|numeric',
            'sp_def'=> 'required|numeric',
            'speed'=> 'required|numeric',
            'generation'=> 'required|numeric',
            'legendary'=> 'required|boolean',
        ];
    }
}
