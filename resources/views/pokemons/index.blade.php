<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Api test Pokemon</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Pokemon Index
        </div>

        <div class="links">
            <a href="{{ route('pokemons.create') }}">Create</a>
            <table>
                <tr>
                    <th>Id</th>
                    <th>No</th>
                    <th>Name</th>
                    <th>Type 1</th>
                    <th>Type 2</th>
                    <th>Total</th>
                    <th>HP</th>
                    <th>Attack</th>
                    <th>Defense</th>
                    <th>Sp. Atk</th>
                    <th>Sp. Def</th>
                    <th>Speed</th>
                    <th>Generation</th>
                    <th>Legendary</th>
                </tr>

            @foreach($collection as $pokemon)

                <tr>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 0)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 1)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 2)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 3)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 4)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 5)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 6)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 7)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 8)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 9)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 10)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 11)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 12)}}</td>
                    <td>{{\Illuminate\Support\Arr::get($pokemon, 13)}}</td>
                    <td><a href="{{ route('pokemons.edit', [$pokemon[0]]) }}">Edit</a></td>
                    <td>
                        <form action="{{ route('pokemons.destroy', [$pokemon[0]]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" >Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </table>
            @if($pageIndex > 1)
                <a href="{{ route('pokemons.index').'?page='.($pageIndex-1) }}" >Previous</a>
            @endif
            @if($pageIndex < $pageCount)
                <a href="{{ route('pokemons.index').'?page='.($pageIndex+1) }}" >Next</a>
            @endif
        </div>
    </div>
</div>
</body>
</html>
