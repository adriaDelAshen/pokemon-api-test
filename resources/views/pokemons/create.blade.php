<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Create pokemon</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Create a pokemon
                    @if($errors)
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    @endif
                    <div>
                        <form action="{{route('pokemons.store')}}" method="POST">
                            @csrf
                            <div>
                                <label for="no">No</label>
                                <input id="no" name="no" type="text" >
                            </div>

                            <div>
                                <label for="name">Name</label>
                                <input id="name" name="name" type="text">
                            </div>
                            <div>
                                <label for="type_1">Type 1</label>
                                <input id="type_1" name="type_1" type="text">
                            </div>
                            <div>
                                <label for="type_2">Type 2</label>
                                <input id="type_2" name="type_2" type="text">
                            </div>
                            <div>
                                <label for="total">Total</label>
                                <input id="total" name="total" type="text">
                            </div>
                            <div>
                                <label for="hp">HP</label>
                                <input id="hp" name="hp" type="text">
                            </div>
                            <div>
                                <label for="attack">Attack</label>
                                <input id="attack" name="attack" type="text">
                            </div>
                            <div>
                                <label for="defense">Defense</label>
                                <input id="defense" name="defense" type="text">
                            </div>
                            <div>
                                <label for="sp_atk">Sp. Atk</label>
                                <input id="sp_atk" name="sp_atk" type="text">
                            </div>
                            <div>
                                <label for="sp_def">Sp. Def</label>
                                <input id="sp_def" name="sp_def" type="text">
                            </div>
                            <div>
                                <label for="speed">Speed</label>
                                <input id="speed" name="speed" type="text">
                            </div>
                            <div>
                                <label for="generation">Generation</label>
                                <input id="generation" name="generation" type="text">
                            </div>
                            <div>
                                <label for="legendary">Legendary</label>
                                <select id="legendary" name="legendary">
                                    <option value="0">False</option>
                                    <option value="1">True</option>
                                </select>
                            </div>
                            <button type="submit" >Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
