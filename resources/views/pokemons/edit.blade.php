<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edit pokemon</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Edit a pokemon
            @if($errors)
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            @endif
            <div>
                <p>Id : {{ $pokemon[0] }}</p>
                <form action="{{route('pokemons.update', [$pokemon[0]])}}" method="POST">
                    <input name="_method" type="hidden" value="PUT">
                    @csrf
                    <div>
                        <label for="no">No</label>
                        <input id="no" name="no" type="text" value={{ $pokemon[1] }}>
                    </div>

                    <div>
                        <label for="name">Name</label>
                        <input id="name" name="name" type="text" value={{ $pokemon[2] }}>
                    </div>
                    <div>
                        <label for="type_1">Type 1</label>
                        <input id="type_1" name="type_1" type="text" value={{ $pokemon[3] }}>
                    </div>
                    <div>
                        <label for="type_2">Type 2</label>
                        <input id="type_2" name="type_2" type="text" value={{ $pokemon[4] }}>
                    </div>
                    <div>
                        <label for="total">Total</label>
                        <input id="total" name="total" type="text" value={{ $pokemon[5] }}>
                    </div>
                    <div>
                        <label for="hp">HP</label>
                        <input id="hp" name="hp" type="text" value={{ $pokemon[6] }}>
                    </div>
                    <div>
                        <label for="attack">Attack</label>
                        <input id="attack" name="attack" type="text" value={{ $pokemon[7] }}>
                    </div>
                    <div>
                        <label for="defense">Defense</label>
                        <input id="defense" name="defense" type="text" value={{ $pokemon[8] }}>
                    </div>
                    <div>
                        <label for="sp_atk">Sp. Atk</label>
                        <input id="sp_atk" name="sp_atk" type="text" value={{ $pokemon[9] }}>
                    </div>
                    <div>
                        <label for="sp_def">Sp. Def</label>
                        <input id="sp_def" name="sp_def" type="text" value={{ $pokemon[10] }}>
                    </div>
                    <div>
                        <label for="speed">Speed</label>
                        <input id="speed" name="speed" type="text" value={{ $pokemon[11] }}>
                    </div>
                    <div>
                        <label for="generation">Generation</label>
                        <input id="generation" name="generation" type="text" value={{ $pokemon[12] }}>
                    </div>
                    <div>
                        <label for="legendary">Legendary</label>
                        <select id="legendary" name="legendary">
                            <option value="0" {{$pokemon[13] == 0 ? 'selected' : ''}} >False</option>
                            <option value="1" {{$pokemon[13] == 1 ? 'selected' : ''}}>True</option>
                        </select>
                    </div>
                    <button type="submit" >Edit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
