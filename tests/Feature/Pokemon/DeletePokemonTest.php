<?php

namespace Tests\Feature\Pokemon;

use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class DeletePokemonTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        //Laravel creates a temporary disk (folder) where it will do the file operations during the test run.
        Storage::fake('local');
        Storage::put('pokemon.csv','Id,#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
1,1,"Bu,lbasaur",Grass,Poison,318,45,49,49,65,65,45,1,False
2,2,Ivysaur,Grass,Poison,405,60,62,63,80,80,60,1,False
3,3,Venusaur,Grass,Poison,525,80,82,83,100,100,80,1,False');
    }

    /**
     * @test
     */
    public function delete_a_pokemon_test()
    {
        /** ARRANGE **/


        /** ACT **/
        $response = $this->json('DELETE', route('pokemons.destroy', [3]));

        /** ASSERT **/
        $response->assertStatus(302);
        $this->assertEquals(Storage::get('pokemon.csv'), 'Id,#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
1,1,"Bu,lbasaur",Grass,Poison,318,45,49,49,65,65,45,1,False
2,2,Ivysaur,Grass,Poison,405,60,62,63,80,80,60,1,False');
    }
}
