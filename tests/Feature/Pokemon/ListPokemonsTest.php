<?php

namespace Tests\Feature\Pokemon;

use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ListPokemonsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        //Laravel creates a temporary disk (folder) where it will do the file operations during the test run.
        Storage::fake('local');
        Storage::put('pokemon.csv','Id,#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
1,1,"Bu,lbasaur",Grass,Poison,318,45,49,49,65,65,45,1,False
2,2,Ivysaur,Grass,Poison,405,60,62,63,80,80,60,1,False
3,3,Venusaur,Grass,Poison,525,80,82,83,100,100,80,1,False');
    }

    /**
     * @test
     */
    public function list_pokemons_test()
    {
        /** ARRANGE **/


        /** ACT **/
        $response = $this->json('GET', route('pokemons.index'));

        /** ASSERT **/
        $response->assertStatus(200);
        $response->assertSee('Ivysaur');
    }
}
