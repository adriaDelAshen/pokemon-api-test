<?php

namespace Tests\Feature\Pokemon;

use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UpdatePokemonTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        //Laravel creates a temporary disk (folder) where it will do the file operations during the test run.
        Storage::fake('local');
        Storage::put('pokemon.csv','Id,#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
1,1,"Bu,lbasaur",Grass,Poison,318,45,49,49,65,65,45,1,False
2,2,Ivysaur,Grass,Poison,405,60,62,63,80,80,60,1,False
3,3,Venusaur,Grass,Poison,525,80,82,83,100,100,80,1,False');
    }

    /**
     * @test
     */
    public function update_a_pokemon_test()
    {
        /** ARRANGE **/


        /** ACT **/
        $response = $this->json('PUT', route('pokemons.update', 1), [
            'no' => 1,
            'name' => 'Pokemon Updated',
            'type_1' => 'Water',
            'type_2' => 'Poison',
            'total' => 54321,
            'hp' => 1,
            'attack' => 1,
            'defense'=> 1,
            'sp_atk'=> 1,
            'sp_def'=> 1,
            'speed'=> 1,
            'generation'=> 1,
            'legendary'=> true,
        ]);

        /** ASSERT **/
        $response->assertStatus(302);
        $this->assertEquals(Storage::get('pokemon.csv'), 'Id,#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
1,1,"Pokemon Updated",Water,Poison,54321,1,1,1,1,1,1,1,1
2,2,Ivysaur,Grass,Poison,405,60,62,63,80,80,60,1,False
3,3,Venusaur,Grass,Poison,525,80,82,83,100,100,80,1,False');

    }

    /**
     * @test
     */
    public function cannot_update_pokemon_if_id_does_not_exist_test()
    {
        /** ARRANGE **/

        /** ACT **/
        $response = $this->json('PUT', route('pokemons.update', 4), [
            'no' => 4,
            'name' => 'Pokemon Updated',
            'type_1' => 'Water',
            'type_2' => 'Poison',
            'total' => 54321,
            'hp' => 1,
            'attack' => 1,
            'defense'=> 1,
            'sp_atk'=> 1,
            'sp_def'=> 1,
            'speed'=> 1,
            'generation'=> 1,
            'legendary'=> true,
        ]);

        /** ASSERT **/
        $response->assertStatus(422);
        $response->json([
            'message' => 'The given data was invalid.',
            'errors' => [
                'id' => [
                    'Id does not exist.'
                ]
            ]
        ]);
    }
}
