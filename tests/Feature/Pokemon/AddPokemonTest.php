<?php

namespace Tests\Feature\Pokemon;

use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AddPokemonTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        //Laravel creates a temporary disk (folder) where it will do the file operations during the test run.
        Storage::fake('local');
        Storage::put('pokemon.csv','Id,#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
1,1,"Bu,lbasaur",Grass,Poison,318,45,49,49,65,65,45,1,False
2,2,Ivysaur,Grass,Poison,405,60,62,63,80,80,60,1,False
3,3,Venusaur,Grass,Poison,525,80,82,83,100,100,80,1,False');
    }

    /**
     * @test
     */
    public function add_a_pokemon_test()
    {
        /** ARRANGE **/


        /** ACT **/
        $response = $this->json('POST', route('pokemons.store', [
            'no' => 4,
            'name' => 'New Pokemon',
            'type_1' => 'Fire',
            'type_2' => 'Grass',
            'total' => 12345,
            'hp' => 130,
            'attack' => 5,
            'defense'=> 5,
            'sp_atk'=> 5,
            'sp_def'=> 5,
            'speed'=> 5,
            'generation'=> 1,
            'legendary'=> false,
        ]));

        /** ASSERT **/
        $response->assertStatus(302);
        $this->assertEquals(Storage::get('pokemon.csv'), 'Id,#,Name,Type 1,Type 2,Total,HP,Attack,Defense,Sp. Atk,Sp. Def,Speed,Generation,Legendary
1,1,"Bu,lbasaur",Grass,Poison,318,45,49,49,65,65,45,1,False
2,2,Ivysaur,Grass,Poison,405,60,62,63,80,80,60,1,False
3,3,Venusaur,Grass,Poison,525,80,82,83,100,100,80,1,False
4,4,"New Pokemon",Fire,Grass,12345,130,5,5,5,5,5,1,0');
    }
}
